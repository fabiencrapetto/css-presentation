let $slider = $('.slider');
let slideCount = 0;


// Reusable elements to create slides
let $slide = $("<div />").addClass("slide"),
    $control = $("<a />").addClass("slider-control"),
    $list = $("<ol />").addClass("table-contents"),
    $item = $("<li />"),
    $link = $("<a />"),
    $bigTitle = $("<h1><span></span></h1>"),
    $title = $("<h2><span></span></h2>"),
    $subTitle = $("<h3><span></span></h3>");

let nameSlide0 = "home",
    hrefSlide0 = "#" + nameSlide0,
    $titleSlide0 = $bigTitle.clone(),
    $slide0,
    hrefPrevSlide = hrefSlide0,
    $prevSlide;

$titleSlide0.children("span").append(TITLE);

let o = (Object.keys(OPTIONS).length === 0 && OPTIONS.constructor === Object)?{}:OPTIONS;

$.fn.hasScrollBar = function() {
    return this.get(0).scrollHeight > this.get(0).offsetHeight;
}
$.fn.slideScrollHandler = function() {
    if($(this).hasScrollBar()){
        let $that = $(this);
        $that.addClass('scrollable');
        $that.on('scroll',function(e){
            if ($(this).hasClass('scrollable')){
                $(this).removeClass('scrollable');
            }
        });
    }
    return $(this);
}

$($slider).addClass('fade-in');
if("animSlide" in o){
    $($slider).addClass(o.animSlide.toLowerCase());
}

if("bgSlidePattern" in o){
    $($slider).addClass(o.bgSlidePattern.toLowerCase());
}

if("bgSlideColors" in o){
    if(Array.isArray(o.bgSlideColors)){
        o.bgSlideColors.forEach( (color,i) => {
            let prop = '';
            switch (i) {
                case 0:
                    prop = '--bg-slide';
                    break;
                case 1:
                    prop = '--bg-slide-dual';
                    break;
                case 2:
                    prop = '--bg-slide-tri';
                    break;
            }
            if(prop != '') document.documentElement.style.setProperty(prop, o.bgSlideColors[i]);
        });
    }else{
        document.documentElement.style.setProperty('--bg-slide', o.bgSlideColors);
    }
}

if("mainColors" in o){
    if(Array.isArray(o.mainColors)){
        o.mainColors.forEach( (color,i) => {
            let prop = '';
            switch (i) {
                case 0:
                    prop = '--color-main';
                    break;
                case 1:
                    prop = '--color-sec';
                    break;
            }
            if(prop != '') document.documentElement.style.setProperty(prop, o.mainColors[i]);
        });
    }else{
        document.documentElement.style.setProperty('--color-main', o.mainColors);
    }
}

$.each(PRESENTATION,function(key,value){
    if(slideCount == 0){
        // Prepares first slide with title and table of contents
        let nameSlide1 = key,
            hrefSlide1 = "#" + nameSlide1;

        $slide0 = $slide.clone()
            .attr("id",nameSlide0)
            .addClass("text-center")
            .append(
                $titleSlide0,
                $list.clone(),
                $control.clone().addClass("next").attr("href", hrefSlide1)
            );
        $("title").append(TITLE);
        $($slider).append(
            $slide0,
            "<!-- \\\\ SLIDE " + slideCount + " // -->"
            );
        $prevSlide = $(hrefSlide0);
    }
    slideCount++;

    let nameCurSlide = key,
        hrefCurSlide = "#" + nameCurSlide,
        titleCurSlide = value.title,
        hasSubList = value.subTable !== undefined,
        isEnd = value.isEnd !== undefined? true:false,
        slideClasses = value.slideClasses !== undefined? value.slideClasses: "",
        $titleCurSlide = hasSubList?$title.clone():$subTitle.clone(),
        content = value.content !== undefined? value.content: "";
        
    $titleCurSlide = isEnd?$bigTitle.clone():$titleCurSlide;

    $titleCurSlide.children("span").append(titleCurSlide);

    // Fill main list table contents
    if(!isEnd){
        $slide0.children('.table-contents').append(
            $item.clone().append(
                $link.clone().append(titleCurSlide).attr("href",hrefCurSlide)
            )
        );
    }
    // Create new slide
    let $curSlide = $slide.clone()
        .attr("id",nameCurSlide)
        .addClass(slideClasses)
        .append(
            $titleCurSlide,
            (hasSubList?$list.clone():content),
            $control.clone().addClass("prev").attr("href", hrefPrevSlide)
        );

    // Add next control to previous slide and prepend new slide before previous slide
    $prevSlide.append(
        $control.clone().addClass("next").attr("href",hrefCurSlide)
    ).before(
        $curSlide,
        "<!-- \\\\ SLIDE " + slideCount + " // -->"
    );

    $prevSlide = $curSlide;
    hrefPrevSlide = hrefCurSlide;    

    if(hasSubList){
        let $parentSlide = $curSlide,
            subTable = value.subTable;

        $.each(subTable,function(subk,subv){
            slideCount++;

            let subName = subk,
                subHref = "#" + subName,
                subTitle = subv.title,
                subContent = subv.content,
                subSlideClasses = subv.slideClasses !== undefined? subv.slideClasses: "",
                $subTitleCurSlide = $subTitle.clone();

                $subTitleCurSlide.children("span").append(
                    $link.clone().attr("href",hrefCurSlide).append(titleCurSlide),
                    " > ",
                    "<small>" + subTitle + "</small>")

                // Fill cur sublist table contents
                $parentSlide.children('.table-contents').append(
                    $item.clone().append(
                        $link.clone().append(subTitle).attr("href",subHref)
                    )
                );
                
            let $subSlide = $slide.clone()
                .attr("id",subName)
                .addClass(subSlideClasses)
                .append(
                    $subTitleCurSlide,
                    subContent,
                    $control.clone().addClass("prev").attr("href", hrefPrevSlide)
                );

            $prevSlide.append(
                $control.clone().addClass("next").attr("href",subHref)
            ).before(
                $subSlide,
                "<!-- \\\\ SLIDE " + slideCount + " // -->"
            );        

            $prevSlide = $subSlide;
            hrefPrevSlide = subHref;

        });
    }
});

$(document).ready(function(){
    $("body").keydown(function(e) {
        if(e.keyCode == 37 && $slider.find('.slider-control.prev:visible')[0]) { // left
            $slider.find('.slider-control.prev:visible')[0].click();
        }
        else if(e.keyCode == 39 && $slider.find('.slider-control.next:visible')[0]) { // right
            $slider.find('.slider-control.next:visible')[0].click();
        }
    });
    if ("onhashchange" in window) {
        window.onhashchange = function () {
            $(".slide:visible").slideScrollHandler().scrollTop(0);
        }
    }
        
    $(".slide:visible").slideScrollHandler().scrollTop(0);
});

